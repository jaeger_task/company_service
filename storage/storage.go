package storage

import (
	"context"
	"errors"
	pb "company_service/genproto/company_service"
)

var ErrorTheSameId = errors.New("cannot use the same uuid for 'id' and 'parent_id' fields")
var ErrorProjectId = errors.New("not valid 'project_id'")

type StorageI interface {
	Company() CompanyI
}

type CompanyI interface {
	Create(ctx context.Context, entity *pb.CreateCompanyRequest) (id string, err error)
	GetAll(ctx context.Context, req *pb.CompanyGettAllRequest) (*pb.CompanyGettAllResponse, error)
	Get(ctx context.Context,req *pb.CompanyId)(*pb.Company,error)
	Update(ctx context.Context,req *pb.Company)(id string, err error)
	Delete(ctx context.Context,req *pb.CompanyId)(error)
}