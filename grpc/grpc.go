package grpc

import (
	"company_service/config"
	"company_service/genproto/company_service"
	"company_service/grpc/service"
	"company_service/pkg/logger"
	"company_service/storage"

	otgrpc "github.com/opentracing-contrib/go-grpc"
	"github.com/opentracing/opentracing-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer(
		grpc.UnaryInterceptor(
			otgrpc.OpenTracingServerInterceptor(opentracing.GlobalTracer())),
		grpc.StreamInterceptor(
			otgrpc.OpenTracingStreamServerInterceptor(opentracing.GlobalTracer())),
	)

	company_service.RegisterCompanyServiceServer(grpcServer, service.NewCompanyService(cfg, log, strg))

	reflection.Register(grpcServer)
	return
}